<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;

class GenerateTokenController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Generate token
    |--------------------------------------------------------------------------
    */
    public function index()
    {
        $request_url = "https://rmcsapitest.azurewebsites.net/token";
        $ch =   curl_init($request_url);  
                curl_setopt($ch, CURLOPT_POST, true );  
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "username=mftlussd&password=wmmTJVn=&grant_type=password");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/x-www-form-urlencoded'
                ));

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $response = json_decode($result);

        if ($error) 
        {
        	logger()->error($error);
        }
        else
        {
            $token = Token::first();

            if ($token) {
                Token::first()->update(['access_token' => $response->access_token]);
            }
            else
            {
                Token::create([
                    'access_token' => $response->access_token
                ]);
            }
        }
    }
}