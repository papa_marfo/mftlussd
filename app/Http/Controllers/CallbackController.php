<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Token;

class CallbackController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Callback
    |--------------------------------------------------------------------------
    */
    public function index(Request $request)
    {
        // get response from xchange
	    $transaction_id = $request->get('clientTransactionID');
	    $status = $request->get('transactionStatus');

        // save transaction status
        Payment::where('transaction_id', $transaction_id)->update([
            'status' => strtolower($status)
        ]);

	    if ($status === 'COMPLETED')
	    {
	    	// get user details from payments and send the data to IRMCS
            $payment = Payment::where('transaction_id', $transaction_id)->first();

            // check if users transaction exists
            if ($payment)
            {
                $this->sendDataToIRMCS($payment->customer_name, $payment->customer_number, $payment->amount, $payment->msid);
            }
	    }
    }

    /*
    |--------------------------------------------------------------------------
    | Save data to IRMCS
    |--------------------------------------------------------------------------
    */
    public function sendDataToIRMCS($customer_name, $customer_number, $amount, $msid)
    {
        $token = Token::first()->value('access_token'); // get token for rmcs
        date_default_timezone_set("Africa/Accra"); // set timezone

        $data = [
            "AccNum" => "1040044422112401",
            "Anonymous" => 0,
            "ContPerson" => $customer_name,
            "ContPhone" => $customer_number,
            "AmountPaid" => $amount,
            "MSID" => $msid,
            "DeviceID" => "USSD",
            "CurID" => 1,
            "Passcode" => 9280,
            "PaymentModeID" => 4,
            "Source" => "USSD",
            "Username" => "mftlussd",
            "syncDate" => date('M d, Y h:i:s A')
        ];

        $request_url = "https://rmcsapitest.azurewebsites.net/api/payment";
        $payload = json_encode($data);

        $ch =   curl_init($request_url);
                curl_setopt($ch, CURLOPT_POST, true );  
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer ' . $token,
                    'Cache-Control: no-cache',
                    'Content-Type: application/json',
                  ));

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $response = json_decode($result);

        if ($error) 
        {
            logger()->error($error);
        }
        else
        {
            logger()->info($result);
        }
    }
}