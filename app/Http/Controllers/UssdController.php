<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\ApiCallsTrait;
use App\Http\Traits\MomoPaymentTrait;
use App\UssdUser;

class UssdController extends Controller
{
	use ApiCallsTrait;
	use MomoPaymentTrait;

    public function index()
    {
    	$ussdRequest = json_decode(@file_get_contents('php://input'));
    	$ussdResponse = [];

    	// check if request != null
    	if ($ussdRequest != NULL)
    	{
    		// get the request type
    		switch ($ussdRequest->Type) 
	    	{
	    		// initial request
	    		case 'Initiation':
	    			// check if network is vodafone
	    			if (strtolower($ussdRequest->Operator) === 'vodafone')
	    			{
	    				$ussdResponse['Message'] = "Please Enter Your Vodafone Voucher Code";

						UssdUser::create([
							'session_id' => $ussdRequest->SessionId,
							'customer_number' => $ussdRequest->Mobile,
							'customer_network' => $ussdRequest->Operator,
						]);

						$ussdResponse['ClientState'] = 'vodafone-menu';
						$ussdResponse['Type'] = 'Response';
	    			}
	    			else
	    			{
	    				$ussdResponse['Message'] = "Welcome to MFTL\n\n1. Funeral Donation\n2. General Payment\n3. Contact Us";

						UssdUser::create([
							'session_id' => $ussdRequest->SessionId,
							'customer_number' => $ussdRequest->Mobile,
							'customer_network' => $ussdRequest->Operator,
						]);

						$ussdResponse['ClientState'] = 'select-options';
						$ussdResponse['Type'] = 'Response';
	    			}
	    		break;

	    		// response
	    		case 'Response':
	    			switch ($ussdRequest->Sequence)
	    			{
    					case 2:
    						switch ($ussdRequest->ClientState)
    						{
    							case 'vodafone-menu':
    								$ussdResponse['Message'] = "Welcome to MFTL\n\n1. Select Services\n2. Contact Us";

									// save all services
    								UssdUser::where('session_id', $ussdRequest->SessionId)
    								->update([
	    								'voucher' => $ussdRequest->Message
	    							]);

									$ussdResponse['ClientState'] = 'select-options';
									$ussdResponse['Type'] = 'Response';
    							break;

    							case 'select-options':
    								// check if input === 1
    								if ($ussdRequest->Message === '1')
    								{
    									// get list of funeral services
			    						$services = $this->getFuneralServices();
			    				
			    						// check if services exist
			    						if ($services)
			    						{
			    							$ussdResponse['Message'] = "Select Funeral Services\n\n";

			    							$data = [];

			    							foreach ($services as $key => $value) 
			    							{
			    								// limit search to funeral
			    								if ($value->ServiceName == "Funeral")
										       	{
										           	$ussdResponse['Message'] .= $key + 1 . ". " . $value->MSNAME . "\n";
										           	$data[] = $value;
										       	}
			    							}

			    							// save all services
		    								UssdUser::where('session_id', $ussdRequest->SessionId)
		    								->update([
			    								'payload' => json_encode($data)
			    							]);

					    					$ussdResponse['ClientState'] = 'enter-name';
					    					$ussdResponse['Type'] = 'Response';
			    						}
				    					else
					    				{
					    					$ussdResponse['Message'] = 'No funeral services available';
											$ussdResponse['Type'] = 'Release';
					    				}
    								}
    								elseif ($ussdRequest->Message === '2')
    								{
    									$ussdResponse['Message'] = "Sorry, this service is unavailable at the moment.";
										$ussdResponse['Type'] = 'Release';
    								}
    								else
    								{
    									$ussdResponse['Message'] = "Contact Mayfair Technologies \n\nCall: 0302917250 \nEmail: info@mayfairtechnologies.com.gh";
										$ussdResponse['Type'] = 'Release';
    								}
    							break;
    							
    							default:
    								$ussdResponse['Message'] = 'Invalid selection.';
    								$ussdResponse['Type'] = 'Release';
    							break;
    						}
	    				break;

	    				case 3:
	    					switch ($ussdRequest->ClientState)
	    					{
	    						case 'select-options':
    								// check if input === 1
    								if ($ussdRequest->Message === '1')
    								{
    									// get list of funeral services
			    						$services = $this->getFuneralServices();
			    				
			    						// check if services exist
			    						if ($services)
			    						{
			    							$ussdResponse['Message'] = "Select Services\n\n";

			    							$data = [];

			    							foreach ($services as $key => $value) 
			    							{
			    								// limit search to funeral
			    								if ($value->ServiceName == "Funeral")
										       	{
										           	$ussdResponse['Message'] .= $key + 1 . ". " . $value->MSNAME . "\n";
										           	$data[] = $value;
										       	}
			    							}

			    							// save all services
		    								UssdUser::where('session_id', $ussdRequest->SessionId)
		    								->update([
			    								'payload' => json_encode($data)
			    							]);

					    					$ussdResponse['ClientState'] = 'enter-name';
					    					$ussdResponse['Type'] = 'Response';
			    						}
				    					else
					    				{
					    					$ussdResponse['Message'] = 'No funeral services available';
											$ussdResponse['Type'] = 'Release';
					    				}
    								}
    								else
    								{
    									$ussdResponse['Message'] = "Contact Mayfair Technologies \n\nCall: 0302917250 \nEmail: info@mayfairtechnologies.com.gh";
										$ussdResponse['Type'] = 'Release';
    								}
    							break;

	    						// enter name
	    						case 'enter-name':
	    							// get the appropriate MSNAME and MSID
	    							$services = UssdUser::where('session_id', $ussdRequest->SessionId)->value('payload');

	    							// parse json
	    							$parsedServices = json_decode($services);

	    							// loop through services and select 
	    							foreach ($parsedServices as $key => $value) {
	    								// Cast before use
	    								$parsedValue = (array) $value;

	    								if (($key + 1) == $ussdRequest->Message) {
	    									$selectedService =  $parsedValue['MSID'];

	    									UssdUser::where('session_id', $ussdRequest->SessionId)
    										->update([
    											'payload' => $selectedService
    										]);
	    								}
	    							}

	    							$ussdResponse['Message'] = "Please enter your name\n";
			    					$ussdResponse['Type'] = 'Response';
	    							// save users input
		    						$ussdResponse['ClientState'] = 'enter-amount';
	    						break;
	    						
	    						default:
	    							$ussdResponse['Message'] = 'Invalid selection.';
	    							$ussdResponse['Type'] = 'Release';
	    						break;
	    					}
	    				break;

	    				case 4:
	    					switch ($ussdRequest->ClientState)
	    					{
	    						// enter name
	    						case 'enter-name':
	    							// get the appropriate MSNAME and MSID
	    							$services = UssdUser::where('session_id', $ussdRequest->SessionId)->value('payload');

	    							// parse json
	    							$parsedServices = json_decode($services);

	    							// loop through services and select 
	    							foreach ($parsedServices as $key => $value) {
	    								// Cast before use
	    								$parsedValue = (array) $value;

	    								if (($key + 1) == $ussdRequest->Message) {
	    									$selectedService =  $parsedValue['MSID'];

	    									UssdUser::where('session_id', $ussdRequest->SessionId)
    										->update([
    											'payload' => $selectedService
    										]);
	    								}
	    							}

	    							$ussdResponse['Message'] = "Please enter your name\n";
			    					$ussdResponse['Type'] = 'Response';
	    							// save users input
		    						$ussdResponse['ClientState'] = 'enter-amount';
	    						break;

	    						// enter amount
	    						case 'enter-amount':
	    							// update user name
		    						UssdUser::where('session_id', $ussdRequest->SessionId)->update([
		    							'customer_name' => ucfirst($ussdRequest->Message)
		    						]);

	    							$ussdResponse['Message'] = "Please enter amount. e.g. 1.00\n";

			    					$ussdResponse['Type'] = 'Response';
		    						$ussdResponse['ClientState'] = 'payment-summary';
	    						break;
	    						
	    						default:
	    							$ussdResponse['Message'] = 'Invalid selection.';
	    							$ussdResponse['Type'] = 'Release';
	    						break;
	    					}
	    				break;

	    				case 5:
	    					switch ($ussdRequest->ClientState)
	    					{
	    						// enter amount
	    						case 'enter-amount':
	    							// update user name
		    						UssdUser::where('session_id', $ussdRequest->SessionId)->update([
		    							'customer_name' => ucfirst($ussdRequest->Message)
		    						]);

	    							$ussdResponse['Message'] = "Please enter amount. e.g. 1.00\n";

			    					$ussdResponse['Type'] = 'Response';
		    						$ussdResponse['ClientState'] = 'payment-summary';
	    						break;

	    						// payment summary
	    						case 'payment-summary':
	    							// check if amount is numeric
	    							if (is_numeric($ussdRequest->Message))
	    							{
	    								// update user name
			    						UssdUser::where('session_id', $ussdRequest->SessionId)->update([
			    							'amount' => $ussdRequest->Message
			    						]);

		    							// get details from db
		    							$user = UssdUser::where('session_id', $ussdRequest->SessionId)->first();

		    							$ussdResponse['Message'] = "Payment Summary\n\n" . "Name: " . $user->customer_name . "\nAmount: GHS" . number_format($user->amount + 0.50, 2) . "\nFee: GHS0.50\n\n1. Continue\n2. Cancel \n\nPowered by MFTL";

				    					$ussdResponse['Type'] = 'Response';
			    						$ussdResponse['ClientState'] = 'make-payment';
	    							}
	    							else
	    							{
	    								$ussdResponse['Message'] = 'Sorry, the amount you entered is not valid.';
	    								$ussdResponse['Type'] = 'Release';
	    							}
	    						break;
	    						
	    						default:
	    							$ussdResponse['Message'] = 'Invalid selection.';
	    							$ussdResponse['Type'] = 'Release';
	    						break;
	    					}
	    				break;

	    				case 6:
	    					switch ($ussdRequest->ClientState)
	    					{
	    						// payment summary
								case 'payment-summary':
									// check if amount is numeric
									if (is_numeric($ussdRequest->Message))
									{
										// update user name
			    						UssdUser::where('session_id', $ussdRequest->SessionId)->update([
			    							'amount' => $ussdRequest->Message
			    						]);

										// get details from db
										$user = UssdUser::where('session_id', $ussdRequest->SessionId)->first();

										$ussdResponse['Message'] = "Payment Summary\n\n" . "Name: " . $user->customer_name . "\nAmount: GHS" . number_format($user->amount + 0.50, 2) . "\nFee: GHS0.50\n\n1. Continue\n2. Cancel \n\nPowered by MFTL";

				    					$ussdResponse['Type'] = 'Response';
			    						$ussdResponse['ClientState'] = 'make-payment';
									}
									else
									{
										$ussdResponse['Message'] = 'Sorry, the amount you entered is not valid.';
										$ussdResponse['Type'] = 'Release';
									}
								break;

	    						// make payment
	    						case 'make-payment':
	    							if ($ussdRequest->Message === '1') 
	    							{
	    								$pid = pcntl_fork();
								        if ($pid === -1) {
								            exit; // failed to fork
								        } 
								        elseif ($pid === 0)
								        {
								            // get details from db
		    								$user = UssdUser::where('session_id', $ussdRequest->SessionId)->first();

		    								$customer_number = preg_replace("/233/", "0", $user->customer_number);
		    								$customer_network = strtoupper($user->customer_network);
		    								$amount = number_format($user->amount + 0.50, 2);
		    								$msid = $user->payload;
											$voucher = $user->voucher;

		    								// make MOMO payment
		    								$this->processMomoPayment($user->customer_name, $customer_number, $customer_network, $amount, $msid, $voucher);
								        } 
								        else
								        {
								            if (strtolower($ussdRequest->Operator) === 'mtn')
		    								{
		    									$ussdResponse['Message'] = "To authorize your MTN transaction, Dial *170#, Select option 7 (Wallet), Select option 3 (My Approvals), Enter your MOMO Pin, Follow the prompts.";
				    							$ussdResponse['Type'] = 'Release';
		    								}
		    								else
		    								{
		    									$ussdResponse['Message'] = "Transaction initiated successfully.";
				    							$ussdResponse['Type'] = 'Release';
		    								}
								        }
	    							}
	    							elseif($ussdRequest->Message === '2')
	    							{
	    								$ussdResponse['Message'] = "Transaction cancelled successfully.";
			    						$ussdResponse['Type'] = 'Release';
	    							}
	    							else
	    							{
	    								$ussdResponse['Message'] = 'Invalid selection.';
	    								$ussdResponse['Type'] = 'Release';
	    							}
	    						break;
	    						
	    						default:
	    							$ussdResponse['Message'] = 'Invalid selection.';
	    							$ussdResponse['Type'] = 'Release';
	    						break;
	    					}
	    				break;

	    				case 7:
			    			switch ($ussdRequest->ClientState)
			    			{
				    			// make payment
								case 'make-payment':
									if ($ussdRequest->Message === '1')
									{
										$pid = pcntl_fork();
										if ($pid === -1) {
										    exit; // failed to fork
										} 
										elseif ($pid === 0)
										{
										   if (strtolower($ussdRequest->Operator) === 'mtn')
											{
												$ussdResponse['Message'] = "To authorize your MTN transaction, Dial *170#, Select option 7 (Wallet), Select option 3 (My Approvals), Enter your MOMO Pin, Follow the prompts.";
				    							$ussdResponse['Type'] = 'Release';
											}
											else
											{
												$ussdResponse['Message'] = "Transaction initiated successfully.";
				    							$ussdResponse['Type'] = 'Release';
											} 
										}
										else
										{
											// get details from db
											$user = UssdUser::where('session_id', $ussdRequest->SessionId)->first();

											$customer_number = preg_replace("/233/", "0", $user->customer_number);
											$customer_network = strtoupper($user->customer_network);
											$amount = number_format($user->amount + 0.50, 2);
											$msid = $user->payload;
											$voucher = $user->voucher;

											// make MOMO payment
											$this->processMomoPayment($user->customer_name, $customer_number, $customer_network, $amount, $msid, $voucher);
										}
									}
									elseif($ussdRequest->Message === '2')
									{
										$ussdResponse['Message'] = "Transaction cancelled successfully.";
			    						$ussdResponse['Type'] = 'Release';
									}
									else
									{
										$ussdResponse['Message'] = 'Invalid selection.';
										$ussdResponse['Type'] = 'Release';
									}
								break;

								default:
									$ussdResponse['Message'] = 'Invalid selection.';
									$ussdResponse['Type'] = 'Release';
								break;
							}
			    		break;
	    				
	    				default:
							$ussdResponse['Message'] = 'Unexpected request.';
							$ussdResponse['Type'] = 'Release';
						break;
	    			}
	    		break;

	    		default:
    				$ussdResponse['Message'] = 'Oops. nothing to show here!!';
					$ussdResponse['Type'] = 'Release';
    			break;
	    	}
    	}
    	else
    	{
			$ussdResponse['Message'] = 'Invalid USSD request.';
			$ussdResponse['Type'] = 'Release';
    	}

    	header('Content-type: application/json; charset=utf-8');
		echo json_encode($ussdResponse);	
    }
}