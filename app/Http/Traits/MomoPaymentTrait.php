<?php

namespace App\Http\Traits;
use Carbon\Carbon;
use App\Payment;
use App\Token;

trait MomoPaymentTrait
{
    /*
    |--------------------------------------------------------------------------
    | Save payment
    |--------------------------------------------------------------------------
    */
    public function processMomoPayment($customer_name, $customer_number, $customer_network, $amount, $msid, $voucher)
    {
        // generate transaction id
        for ($transaction_id = mt_rand(1, 9), $i = 1; $i < 10; $i++)
        {
            $transaction_id .= mt_rand(0, 9);
        }

        // prepare payload for xchange
        $payment = new Payment;
        $payment->transaction_id = $transaction_id;
        $payment->customer_name = $customer_name;
        $payment->customer_number = $customer_number;
        $payment->customer_network = $customer_network;
        $payment->amount = $amount;
        $payment->msid = $msid;

        // save transaction details
        if ($payment->save())
        {
            logger()->info('Payment saved!!');

            // call api to debit money from the customer
            $this->debitMoneyFromCustomer($payment->transaction_id, $payment->customer_number, $payment->customer_network, $payment->amount, $voucher);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Debit money from customer
    |--------------------------------------------------------------------------
    */
    public function debitMoneyFromCustomer($transaction_id, $customer_number, $customer_network, $amount, $voucher)
    {
        logger()->info('Debit money from customer!!');

        // get token for rmcs
        $token = Token::first()->value('access_token');

        $data = [
           'trans_id' => $transaction_id,
           'mobile_number' => $customer_number,
           'mobile_network' => $customer_network,
           'amount' => $amount,
           'callback' => 'https://mftl.herokuapp.com/ussd/callback',
           'voucher_no' => $voucher ? $voucher : ''
        ];

        $request_url = "https://momoapi.azurewebsites.net/api/v1/momo/debit";
        $payload = json_encode($data);
        $ch =   curl_init($request_url);  
                curl_setopt($ch, CURLOPT_POST, true);  
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer ' . $token,
                    'Cache-Control: no-cache',
                    'Content-Type: application/json',
                  ));

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $response = json_decode($result);

        if ($error) 
        {
            logger()->error($error);
        }
        else
        {
            logger()->info($result);
        }
    }
}