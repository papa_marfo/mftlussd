<?php

namespace App\Http\Traits;
use App\Token;

trait ApiCallsTrait 
{
    /*
    |--------------------------------------------------------------------------
    | Get list of funeral services
    |--------------------------------------------------------------------------
    */
    public function getFuneralServices()
    {
        // get token for rmcs
        $token = Token::first()->value('access_token');

    	$data = [
           'actionCode' => 101,
           'serviceID' => 2 
        ];

        $request_url = "https://rmcsapitest.azurewebsites.net/api/service";
        $payload = json_encode($data);
        $ch =   curl_init($request_url);  
                curl_setopt($ch, CURLOPT_POST, true );  
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                	'Authorization: Bearer ' . $token,
                    'Cache-Control: no-cache',
                    'Content-Type: application/json',
                  ));

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $response = json_decode($result);

        if ($error) 
        {
        	logger()->error($error);
        }
        else
        {
        	$services_list = $response->locationService;
        	return $services_list; // return list of services
        }
    }
}