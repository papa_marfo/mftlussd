<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UssdUser extends Model
{
    protected $fillable = [
    	'session_id',
    	'customer_name',
    	'customer_number',
    	'customer_network',
    	'amount',
    	'msid'
    ];
}