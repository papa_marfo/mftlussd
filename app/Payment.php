<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
    	'transaction_id',
    	'customer_name',
    	'customer_number',
    	'customer_network',
    	'response'
    ];
}